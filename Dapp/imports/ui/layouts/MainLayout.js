import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Menu from '../components/Menu.js';

// pages ...
import Home from '../pages/Home.js';
import About from '../pages/About.js';
import Howtoplay from '../pages/Howtoplay.js';
import Drawing from '../pages/Drawing.js';

// css ...
import '../stylesheets/Menu.scss' 

export default class MainLayout extends Component {
  render(){
    return(
      <div>
        <BrowserRouter>
        <div>
            <Menu />
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/about" component={About}/>
              <Route path="/howtoplay" component={Howtoplay}/>
              <Route path="/drawing" component={Drawing} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}