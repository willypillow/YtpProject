import React, { Component } from 'react';
import { Address, myContract, contractInstance } from '../../startup/client/contract.js';
import { accountsData } from '../../api/accounts.js';
import { BettingData } from '../components/Bettingdata.js'
//import { formatBalance } from 'ethtools';
import { clearInterval, setInterval } from 'timers';
import '../stylesheets/Drawing.scss';

const debug = ["bidding", "reveal", "waitDraw", "ForceEnd"];
const state = ["等待投注", "等待隨機數生成", "等待開獎", "錯誤"];
function Promisify(){
  const promisify = (inner) =>
    new Promise((resolve, reject) =>
      inner((err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    );
  return promisify;
}
export default class Drawing extends Component{
  constructor(props){
    super(props);
    this.state = ({
      curState:4,
      blocks: 50,
    })
  }
  getBlocks(){
    let _this = this;
    this.timer = setInterval(this.update.bind(this), 500);
  }
  displayPopup() {
    document.getElementById('draw-video').play();
    document.getElementById('popup').style.display = 'block';
  }
  closePopup() {
    document.getElementById('draw-video').pause();
    document.getElementById('popup').style.display = 'none';
  }
  reveal(){
    let coinBase = web3.eth.coinbase; 
    let seed = accountsData.get(coinBase).seed;
    console.log(seed);
    contractInstance.reveal(seed, {
      from: coinBase,
      gas: 6000000,
    }, function(err, res){
      if(err) throw err;
      console.log('revealed!');
    });
  }
  getAnswer(cb){
    //TODO: get ans array
    let _this = this;
    contractInstance.ans(function(err, ans) {
      console.log(ans);
      let ansArr = []
      for (i = 1; i <= 6; i++) {
        ansArr.push(parseInt(ans.substr(i * 2, 2), 16));
      }
      _this.setState({
        curState: 4,
        blocks:-1,
        ans: ansArr,
      });
      cb();
    });
  }
  getBalance(cb) {
    let _this = this;
    contractInstance.prize(web3.eth.coinbase, function(err, balance) {
      _this.state.balance = balance;
      cb();
    });
  }
  update(){
    const promisify = Promisify();
    let _this = this;
    web3.eth.getBlockNumber(function(err, number) {
      let p0 = promisify(cb => contractInstance.startTime(cb));
      let p1 = promisify(cb => contractInstance.duration(cb));
      let p2 = promisify(cb => contractInstance.durReveal(cb));
      let p3 = promisify(cb => contractInstance.durForceEnd(cb));
      let p4 = promisify(cb => contractInstance.lastDrawn(cb));
      Promise.all([p0, p1, p2, p3, p4]).then((res) => {
        res = res.map((x) => x.toNumber());
        number -= res[0];
        if (number <= res[1]) {
          _this.setState({
            curState: 0,
            blocks: res[1] - number,
          });
        } else if (number <= res[2]) {
          _this.setState({
            curState: 1,
            blocks: res[2] - number,
          });
          // TODO: Call reveal
          if (!_this.state.revealed) {
            _this.reveal();
            _this.state.revealed = true;
          }
        } else if (res[4] != res[0]) {
          if (number <= res[3]) {
            _this.setState({
              curState: 2,
              blocks: res[3] - number,
            });
          } else {
            _this.setState({
              curState: 3,
              blocks: 0,
            });
          }
        } else {
          clearInterval(_this.timer);
          _this.displayPopup();
          _this.getAnswer(() =>
            _this.getBalance(() =>
              setTimeout(_this.closePopup, 1000)));
        }
        console.log(debug[_this.state.curState]);
        console.log(_this.state.curState);
      });
    });
  }
  componentWillMount(){
    this.getBlocks();
    this.setState({ans:[1, 2, 3, 4, 5, 6]});
    EthTools.ticker.start();
  }
  render(){
    let account = accountsData.get(web3.eth.coinbase);
    let temp = [];
    if (typeof account != 'undefined') temp = account.tickets;
    const round = (x) => Math.round(x * 100) / 100;
    let balance = parseFloat(web3.fromWei(this.state.balance, "ether"));
    let usdRate = EthTools.ticker.findOne('usd').price;
    let toUsd = balance * usdRate;

    return (
        <div>
          <div className="popup" id="popup">
            <div className="popup-contents">
              <video className="draw-video" id="draw-video" src="/drawVideo.webm" loop="true" />
            </div>
          </div>
          {
            this.state.curState != 4 ? (
              <div className="row">
                <div className="status">
                  <div className="circle"></div>
                  <div className="blocks-left center-align flow-text">{state[this.state.curState]}<br/> 估計時間:{this.state.blocks * 15}s</div>
                </div>
              </div>
            ) : (
              <div>
                <div className="row">
                  <div className="col m12 l7">
                    <div className="heading">
                      <h1>開獎號碼:</h1>
                    </div>
                    <div className="">
                      {
                        this.state.ans.map((item, i) => {
                          return(
                            <div key={i} className="answerBtn btn-large col s1"><span className="flow-text">{item}</span></div>
                          );
                        })
                      }
                    </div>
                  </div>
                  <div className="col m12 l5">
                    <div className="convert center-align flow-text">
                      1 ETH = {usdRate} USD <br/>
                      Total Prize: {round(balance)} ETH ({round(toUsd)} USD)<br/>
                    </div>
                  </div>
                </div>
                <div className="row">
                </div>
              </div>
            )
          }
          <div className="collection choiced-list row">
            { 
                temp.map((item, i) => {
                return (
                  <div key={i} className="flow-text collection-item">
                    {item.join(' ')}
                    <span className="badge"><span className="flow-text">{i}</span></span>
                  </div>
                );
              })
            }
          </div>
        </div>
    );
  }
}
