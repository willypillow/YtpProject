import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export default class Menu extends Component{
  render(){
    return(
      <div className="navbar-fixed scrollspy">
        <nav>
         <div className="nav-wrapper">
            <Link to='/' className="brand-logo left main-text">DLottery</Link>
            <ul className="right">
							<a href="#" className="dropdown-button" data-activates="hamburger" data-constrainwidth="false" data-beloworigin="true">&#9660;</a>
							<ul id="hamburger" className="dropdown-content">
                  <li><Link to='/drawing'> Drawing </Link></li>
									<li><Link to='/howtoplay'> How To Play? </Link></li>
									<li><Link to='/about'> About </Link></li>
							</ul>
						</ul>
          </div>
        </nav>
      </div>
    );
   }
}
