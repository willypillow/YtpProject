import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor' 
import { Address, myContract, contractInstance} from '../../startup/client/contract.js';
import { Router, Route, Link, RouteHandler } from 'react-router';

import { accountsData } from '../../api/accounts.js';

function arrayToHexString(array) {
  let s = '0x';
  array.forEach(function(byte) {
    s += ('0' + (byte & 0xFF).toString(16)).slice(-2);
  });
  return s;
}

function generateNumber() {
  let array = new Uint8Array(32);
  window.crypto.getRandomValues(array);
  // TODO: Store array
  
  return arrayToHexString(array);
}

export default class Bettingdata extends Component{
  constructor(props){
    super(props);
    this.state = ({
      isSummitted: false,
      basePrice: web3.toBigNumber(0)
    });
    let _this = this;
    contractInstance.price((err, res) =>
      _this.state.basePrice = web3.toBigNumber(res)
    );
  }
  handleSubmit(lists, price){
    console.log('contractInstanceAddress: ' + contractInstance.address);
    const seed = generateNumber();
    const hash = web3.sha3(seed, {encoding:'hex'}); 
    console.log('hash: ' + hash);
    console.log('coin base address: ' + coinBase);
    console.log('price (Wei): ', price);
    lists = lists.map(arrayToHexString);

    let _this = this;
    let tickets = this.props.lists;
    let coinBase = web3.eth.coinbase;

    contractInstance.bid(hash, lists, {
      value: price,
      from: coinBase,
      gas: 1000000,
    }, function(error, result){
      if(error) throw error;
      else{
        console.log(result);
        console.log('finished');
        _this.setState({isSummitted: true});
        /*
        if(!accountsData.has(coinBase)){
          accountsData.set(coinBase, {seed: [web3.toBigNumber(seed)], tickets: tickets}); 
        }
        else{
          let seeds = accountsData.get(coinBase).seed;
          seeds.push(web3.toBigNumber(seed));
          accountsData.set(coinBase, {seed: seeds, tickets: tickets}); 
        }
        */
        accountsData.set(coinBase, {seed: web3.toBigNumber(seed), tickets: tickets}); 
        console.log(accountsData.get(coinBase));
        window.location = '/drawing';
      }
    });
  }
  render(){
    let price = this.state.basePrice.times(this.props.lists.length).toString(10);
    return(
      <div className="collection">
        <div className="flow-text collection-item">
          Price: {web3.fromWei(price, "gwei")} Gwei
          <span className="btn orange white-text badge" onClick={() => this.handleSubmit(this.props.lists, price)}>
           Submit 
          </span>
        </div>
        { 
          this.props.lists.map((item, i) => {
            return (
              <div key={i} className="flow-text collection-item">
                {item.join(' ')}
                <span className="badge"><span className="flow-text">{i + 1}</span></span>
              </div>
            );
          })
        }
      </div>
    ); 
  }
}
