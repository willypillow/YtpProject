import React, { Component } from 'react';
import Bettingdata from '../components/Bettingdata.js'; 
import '../stylesheets/Board.scss';
_ = lodash;
const rowNumber = 7;
const colNumber = 7;
const maxCheck = 6;
class Square extends Component{
  render(){
    let val = this.props.value;
    return(
      <div className="col s1-4">
        <a className={"waves-effect waves-light numberBtn btn-large " +  (this.props.checked ? "orange" : "lime") } onClick={() => this.props.onClick()}>
          <span className="flow-text">
          {
            (val) <= 9 && 
            <span>0</span>
          }
          {val}
          </span>
        </a>
      </div>
    );
  }
} 
export default class Board extends Component{
  constructor(props){
    super(props);
    this.state = ({
        squares: [],
        choices: [],
        tickets: [],
        choicedCount: 0,
        transactionCount: 0,
    });
  }
  componentWillMount(){
    let squares = new Array(rowNumber);
    let tickets = new Array();
    for(let i = 0; i < rowNumber; i++)
      squares[i] = new Array(colNumber);
    this.setState({
      squares: squares,
      tickets: tickets,
    });
  }
  handleClick(i, j){
    // console.log(this.state);
    let squares = this.state.squares.slice();
    let choices = this.state.choices.slice();
    let choicedCount = this.state.choicedCount;
    let val = i * 7 + j + 1;


    if(!squares[i][j] && choicedCount >= maxCheck){
      alert('You are too greedy!!!');
    }
    else{
      squares[i][j] = !squares[i][j];
      if(squares[i][j]){
        choicedCount++;
        choices.push(val);
      }
      else{
        choicedCount--;
        for(let i = 0; i < choices.length; i++){
          if(choices[i] == val){
            delete choices[i]; 
            break;
          }
        }
      }
      choices = _.sortBy(choices);
      this.setState({
        squares: squares,
        choicedCount: choicedCount,
        choices: choices,
      });
    }
  }
  reset(){
    let squares = new Array(rowNumber); 
     for(let i = 0; i < rowNumber; i++)
       squares[i] = new Array(colNumber);
     this.setState({
       choices: [],
       squares: squares,
       choicedCount: 0,
     });
  }
  getRandom(){
    let choices = this.state.choices;
    console.log(choices.length);
    for(let i = choices.length; i < maxCheck; i++){
      while(true){
        let x = _.random(1, rowNumber * colNumber);
        console.log(x);
        let ok = true;
        choices.map((item) => {
          if(item == x) ok = false;
        });
        if(ok){ choices.push(x); break;}
      }
    }
    return _.sortBy(choices);
  }
  addtickets(){
    let choices = this.getRandom();
    let tickets = this.state.tickets.slice();
    this.reset();
    tickets.push(choices);
    this.setState((prevState) => {
      return {
        tickets: tickets,
        transactionCount: prevState.transactionCount + 1,
      };
    });
  }
  render(){
    let rows = _.range(rowNumber).map((i) => {
      let cols = _.range(colNumber).map((j) => {
        let idx = i * 7 + j + 1;
        return (
          <Square 
            key={idx}
            value={idx}
            checked={this.state.squares[i][j]}
            onClick={() => this.handleClick(i, j)}>
          </Square>
        );
      });
      return (
        <div className="row" key={i}> 
          <div className="col s12">
            { cols } 
          </div>
        </div>
      );
    });
    return (
      <div className="container">
        <div className="row">
          <div className="playcontent col m12 l8">
            { rows }
            <div className="row padUp">
              <div className="col s7">
                <span className="flow-text">
                  Selection: 
                  {
                    this.state.choices.filter(item => item).map((i) => {
                      return (' ' + i);
                    })
                  }
                </span>
              </div>
              <div className="waves-effect waves-light btn orange lighten-2 col s2" onClick={this.reset.bind(this)}>
                <span className="flow-text">
                  reset
                </span>
              </div>
              <div className="btn col s3" onClick={this.addtickets.bind(this)}>
                <span className="flow-text">
                  Add ({this.state.transactionCount})
                </span>
              </div>
            </div>
          </div>
          <div className="ticketslist col m12 l4">
            <Bettingdata lists={this.state.tickets}/>
          </div> 
        </div>
      </div>
    );
  }
}
