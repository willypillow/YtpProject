pragma solidity ^0.4.19;
import "./Lotto.sol";
import "./EthLotto.sol";

contract Lotto649 is Lotto, EthLotto {
	event _debug(byte x);
	struct Bid {
		address user;
		bytes6 nums;
	}

	uint[8] public ratio = [uint(100), 100, 200, 300, 500, 700, 1000, 7000];
	uint public price;
	bytes public ans;
	address[][8] public prizeList;
	Bid[] bids;

	function Lotto649(
		bytes32 hash,
		uint _duration,
		uint _durReveal,
		uint _durEnd,
		uint _price
	)
		Lotto(hash, _duration, _durReveal, _durEnd)
		public
	{
		price = _price;
		ans.length = 49;
		for (uint i = 0; i < 49; i++) ans[i] = byte(i + 1);
	}

	function rng(uint range, uint seed) internal pure returns (uint, uint) {
		uint rand = (seed & (2 ** 250 - 1)) * range / (2 ** 250);
		return (rand, uint(keccak256(seed)));
	}
	
	function _draw(uint seed) internal {
		uint j;
		for (uint i = 0; i <= 6; i++) { // Knuth shuffle
			(j, seed) = rng(49 - i, seed);
			(ans[i], ans[i + j]) = (ans[i + j], ans[i]);
		}
		for (i = 0; i < 6; i++) {
			for (j = i + 1; j < 6; j++) {
				if (ans[i] > ans[j]) (ans[i], ans[j]) = (ans[j], ans[i]);
			}
		}
		for (i = 0; i < bids.length; i++) {
			uint b = 0;
			uint matches = 0;
			bool special = false;
			for (uint a = 0; a < 6; a++) {
				while (b < 6 && bids[i].nums[b] < ans[a]) {
					if (bids[i].nums[b] == ans[6]) special = true;
					b++;
				}
				if (b < 6 && bids[i].nums[b] == ans[a]) matches++;
			}
			uint typ = (matches - 2) * 2 + (special ? 1 : 0) - 1;
			if (typ > 7) typ = 7; // Jackpot
			if (typ >= 0) prizeList[typ].push(bids[i].user);
		}
		uint tot = this.balance - totalPrize;
		for (i = 0; i <= 7; i++) {
			if (prizeList[i].length > 0) {
				uint amount = tot * ratio[i] / (10000 * prizeList[i].length);
				for (j = 0; j < prizeList[i].length; j++) {
					Lotto.prize[prizeList[i][j]] += amount;
					totalPrize += amount;
				}
			}
		}
	}

	function bid(bytes32 hash, bytes6[] tickets) external payable {
		uint amount = price * tickets.length;
		require(msg.value >= amount);
		for (uint i = 0; i < tickets.length; i++) {
			for (uint j = 1; j < 6; j++) require(tickets[i][j - 1] < tickets[i][j]);
			for (j = 0; j < 6; j++) _debug(tickets[i][j]);
		}
		super._bid(hash, amount);
		for (i = 0; i < tickets.length; i++) {
			bids.push(Bid(msg.sender, tickets[i]));
		}
	}

	function restart(bytes32 hash) external {
		super._restart(hash);
		bids.length = 0;
		for (uint i = 0; i < 8; i++) prizeList[i].length = 0;
	}

	function reveal(uint seed) external { super._reveal(seed); }
	function endRound(uint seed) external { super._endRound(seed); }
	function withdrawBid() external { super._withdrawBid(); }
	function withdrawPrize() external { super._withdrawPrize(); }
}
