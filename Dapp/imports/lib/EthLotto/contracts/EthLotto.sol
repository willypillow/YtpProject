pragma solidity ^0.4.19;
import "./Lotto.sol";

contract EthLotto is Lotto {
	function _transfer(address to, uint amount) internal { to.transfer(amount); }
}
