pragma solidity ^0.4.19;

contract Lotto {
	struct SeedHash {
		bytes32 hash;
		uint time;
	}

	address public owner;
	uint public startTime;
	uint public duration;
	uint public durReveal;
	uint public durForceEnd;
	uint public lastDrawn;
	uint totalPrize;
	mapping(address => uint) public prize;
	mapping(address => uint) bidAmount;
	bytes32 initHash;
	mapping(address => SeedHash) seedHash;
	mapping(bytes32 => uint) hashUsed;
	uint finalSeed;

	modifier onlyOwner { require(msg.sender == owner); _; }

	modifier canBid {
		require(block.number <= startTime + duration);
		_;
	}

	modifier canReveal {
		require(startTime + duration < block.number &&
				block.number <= startTime + durReveal);
		_;
	}

	modifier canEnd {
		require(block.number > startTime + durReveal);
		_;
	}

	function Lotto(
		bytes32 hash,
		uint _duration,
		uint _durReveal,
		uint _durForceEnd
	) internal
	{
		owner = msg.sender;
		initHash = hash;
		duration = _duration;
		durReveal = _durReveal;
		durForceEnd = _durForceEnd;
		startTime = block.number;
	}

	function _draw(uint seed) internal;
	function _transfer(address to, uint amount) internal;

	function _restart(bytes32 hash) internal onlyOwner {
		require(lastDrawn == startTime && block.number > startTime);
		initHash = hash;
		startTime = block.number;
	}

	function _bid(bytes32 hash, uint amount) internal canBid {
		require(seedHash[msg.sender].time != startTime && hashUsed[hash] != startTime);
		seedHash[msg.sender] = SeedHash(hash, startTime);
		bidAmount[msg.sender] = amount;
		hashUsed[hash] = startTime;
	}

	function _reveal(uint seed) internal canReveal {
		require(seedHash[msg.sender].time == startTime &&
				sha3(seed) == seedHash[msg.sender].hash);
		finalSeed ^= seed;
	}

	function _endRound(uint seed) internal onlyOwner canEnd {
		require(sha3(seed) == initHash);
		finalSeed ^= seed;
		_draw(finalSeed);
		lastDrawn = startTime;
	}

	function _withdrawBid() internal {
		require(block.number > startTime + durForceEnd);
		uint amount = bidAmount[msg.sender];
		bidAmount[msg.sender] = 0;
		_transfer(msg.sender, amount);
	}

	function _withdrawPrize() internal {
		uint amount = prize[msg.sender];
		totalPrize -= amount;
		prize[msg.sender] = 0;
		_transfer(msg.sender, amount);
	}
}
