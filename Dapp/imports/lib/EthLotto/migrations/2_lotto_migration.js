var Lotto649 = artifacts.require("./Lotto649.sol");

module.exports = function(deployer) {
	require('crypto').randomBytes(32, function(err, buf) {
		var randStr = buf.toString('hex');
		require('fs').writeFile("secretSeed", randStr);
		deployer.deploy(Lotto649, web3.sha3(randStr, {encoding: 'hex'}), 2, 4, 30, web3.toWei(10000, "Gwei"));
	});
	/*
	require('crypto').randomBytes(32, function(err, buf) {
		Lotto649.restart(web3.sha3(buf.toString('hex'), {encoding: 'hex'}), {from:web3.eth.coinbase, gas:100000});
	}
	*/

};
