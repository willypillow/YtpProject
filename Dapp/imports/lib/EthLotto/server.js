var address = '0x581da87aa926a4d9b49c863bd2db6ddcb82097d0';
var myContract = web3.eth.contract(Lotto649.abi);
var contractInstance = myContract.at(address);
var promisify = (inner) =>
	new Promise((resolve, reject) =>
		inner((err, res) => {
			if (err) reject(err);
			else resolve(res);
		})
	);
function update() {
	web3.eth.getBlockNumber(function(err, number) {
		let p0 = promisify(cb => contractInstance.startTime(cb));
		let p1 = promisify(cb => contractInstance.duration(cb));
		let p2 = promisify(cb => contractInstance.durReveal(cb));
		let p3 = promisify(cb => contractInstance.durForceEnd(cb));
		let p4 = promisify(cb => contractInstance.lastDrawn(cb));
		Promise.all([p0, p1, p2, p3, p4]).then((res) => {
			res = res.map(x => x.toNumber());
			number -= res[0];
			if (number <= res[1]) {
				console.log('bidding');
			} else if (number <= res[2]) {
				console.log('revealing');
			} else if (number <= res[3]) {
				if (res[4] != res[0]) {
					require('fs').readFile('secretSeed', function(err,seed) {
						if (err) console.log(err);
						contractInstance.endRound("0x" + seed, {
							from: web3.eth.coinbase,
							gas: 4000000
						});
					});
				} else {
					require('crypto').randomBytes(32, function(err, buf) {
						//var randStr = buf.toString('hex');
						//require('fs').writeFile("secretSeed", randStr);
						//contractInstance.restart(web3.sha3(randStr, {encoding: 'hex'}), {
						//	from: web3.eth.coinbase,
						//	gas: 4000000
						//});
					});
				}
			} else {
				console.log('error: forceend');
			}
		});
	});
	setTimeout(update, 1000);
}
update();
