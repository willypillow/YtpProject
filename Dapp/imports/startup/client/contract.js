import { Mongo } from 'meteor/mongo';
import Lottocontract from '../../lib/EthLotto/build/contracts/Lotto649.json';

export const Address = "0x53ea730ea449600e1769cdd9fcf9e0c39d6ebeb7";
export const myContract = web3.eth.contract(Lottocontract.abi);
export const contractInstance = myContract.at(Address);
