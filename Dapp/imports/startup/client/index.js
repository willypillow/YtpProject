import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
 
import MainLayout from '../../ui/layouts/MainLayout.js'

Meteor.startup(() => {
  render(<MainLayout />, document.getElementById('render-target'));
});