#!/bin/sh
pandoc --standalone --to=revealjs --output=slide.html --mathml --variable='revealjs-url:https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0' slide.md
