% DLottery
% 8 + 9 != Choder

# Problem

## ![](1520572109.png)

## Really?
- 大概是假的（？
- 但現有樂透的公正性經常受到質疑

## ![](1520578066.png)
- 期望值頗低？


# Solution

##
- 追求公正性
- 去除中間人

## 區塊鏈
### Blockchain Technology

##
- [&#10003;] 公開
- [&#10003;] 去中心化
- [&#10003;] 不可竄改

## Decentralized Lottery!
- 公正
- 去除中間人 &#8658; 低抽成、手續費
- 方便
- user-friendly


# Team

## 黃于軒
- 智能合約設計、撰寫
- 樂透規則設計
- 系統架構發想
- 區塊鏈技術支援

## 鐘凡
- 網頁界面邏輯
- 區塊鏈 API 互動
- 寄收據

## 楊鈞任
- 網頁 UI CSS 美化
- 網頁 UX 設計
- 開獎動畫製作
- 撰寫網頁說明文字


# How?

## Ethereum
- 以太坊智能合約(smart contract)技術
- Solidity 程式語言

&#8658; 在區塊鏈上構築購買樂透和公平開獎的機制

## Meteor
- 以 Meteor 框架撰寫親民的網頁界面
- 使用者選號、購買
- 自動從區塊鏈上抓取開獎號碼、中獎人…
- Other libraries: React, Materialize...

## Web3
- 智慧合約及使用者界面的橋樑
- Metamask: manage wallets locally


# Demo
 
 
# What's Next?

## 更多種玩法!
- 不同樂透規則
- 其它基於隨機性的遊戲

## ERC20 Tokens
- 發行代幣，可使獎金不限於現有 pool 裡的金額

&#8658; 更豐富的規則

- 代幣持有者可分到一小部份營收？

&#8658; 持有的誘因

## Open Source
- 樂透的核心——智能合約——由於架構在區塊鏈上，代碼已公開
- 但使用者互動的窗口——網頁界面，目前仍為封閉
- 真正的去中心化

## Charity
- 和去年的區塊鏈募捐平台結合，成為慈善機構的資金來源之一？


# Q/A
